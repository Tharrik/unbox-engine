
var canvas;
var ctx;

var pi_2 = Math.PI * 2;

var fixedDeltaTime = 0.01666666; // 60fps: 1 frame each 16.66666ms
var deltaTime = fixedDeltaTime;

var time = 0,
    FPS  = 0,
    frames    = 0,
    acumDelta = 0;

// key events
var lastPress = null;

var KEY_LEFT  = 37, KEY_A = 65;
var KEY_UP    = 38, KEY_W = 87;
var KEY_RIGHT = 39, KEY_D = 68;
var KEY_DOWN  = 40, KEY_S = 83;
var KEY_PAUSE = 19;
var KEY_SPACE = 32;

var keyboard = {};

var mouse = { x: 0, y: 0 };

function Init ()
{
    // preparamos la variable para el refresco de la pantalla
    window.requestAnimationFrame = (function (evt) {
        return window.requestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, fixedDeltaTime * 1000);
            };
    }) ();

    canvas = document.getElementById("my_canvas");
    
    if (canvas.getContext)
    {
        ctx = canvas.getContext('2d');

        PreparePhysics(ctx);

        // first call to the game loop
        Loop();

        // setup keyboard events
        SetupKeyboardEvents();

        // mouse click event
        canvas.addEventListener("mousedown", MouseDown, false);
        // mouse move event
        canvas.addEventListener("mousemove", MouseMove, false);
    }
}

function SetupKeyboardEvents ()
{
    AddEvent(document, "keydown", function (e) {
        keyboard[e.keyCode] = true;
    } );

    AddEvent(document, "keyup", function (e) {
        keyboard[e.keyCode] = false;
    } );

    function AddEvent (element, eventName, func)
    {
        if (element.addEventListener)
            element.addEventListener(eventName, func, false);
        else if (element.attachEvent)
            element.attachEvent(eventName, func);
    }
}

function Loop ()
{
    requestAnimationFrame(Loop);

    var now = Date.now();
    deltaTime = now - time;
    if (deltaTime > 1000) // si el tiempo es mayor a 1 seg: se descarta
        deltaTime = 0;
    time = now;

    frames++;
    acumDelta += deltaTime;

    if (acumDelta > 1000)
    {
        FPS = frames;
        frames = 0;
        acumDelta -= 1000;
    }
    
    // transform the deltaTime from miliseconds to seconds
    deltaTime /= 1000;

    // Game logic -------------------
    Update();

    // Draw the game ----------------
    Draw();
}

function Update ()
{
    // update physics
    // Step(timestep , velocity iterations, position iterations)
    world.Step(deltaTime, 8, 3);
    world.ClearForces();
}

function Draw ()
{
    // clean the canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // draw the background
    DrawBackground();

    // draw the world
    DrawWorld(world);
    
    // draw the FPS
    ctx.fillStyle = "black";
    ctx.fillText('FPS: ' + FPS, 10, 10);
    ctx.fillText('deltaTime: ' + Math.round(1 / deltaTime), 10, 20);
    ctx.fillText('total bodys: ' + world.GetBodyCount(), 10, 30);
}

function DrawBackground ()
{
    // background
    var bgGrd = ctx.createLinearGradient(0, 0, 0, canvas.height);
    bgGrd.addColorStop(0, "black");
    bgGrd.addColorStop(1, "#365B93");
    ctx.fillStyle = bgGrd;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function DrawWorld (world)
{
    // Transform the canvas coordinates to cartesias coordinates
    ctx.save();
    ctx.translate(0, canvas.height);
    ctx.scale(1, -1);
    world.DrawDebugData();
    ctx.restore();
}

function MouseDown (event)
{
    var rect = canvas.getBoundingClientRect();
    var clickX = event.clientX - rect.left;
    var clickY = event.clientY - rect.top;
    //console.log("MouseDown: " + "X=" + clickX + ", Y=" + clickY);

    var forcePower = {x:10, y:100};
    var force = new b2Vec2(
        (physicObjects[0].GetPosition().x - (clickX / scale)),
        (physicObjects[0].GetPosition().y - ((canvas.height - clickY) / scale))
    );
    force.Normalize();
    force.x *= forcePower.x;
    force.y *= forcePower.y;
    physicObjects[0].ApplyForce(force, physicObjects[0].GetPosition());
}

function MouseMove (event)
{
    var rect = canvas.getBoundingClientRect();
    mouse.x = event.clientX - rect.left;
    mouse.y = event.clientY - rect.top;
}
