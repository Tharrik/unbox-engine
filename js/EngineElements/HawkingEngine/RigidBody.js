/**
 * This class represents ...
 */

function RigidBody(actor){
    // STATIC MEMBERS ----------------------------------------------------------------------------//

    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    
    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var that = this;

    var hawking;

    var owner;
    var collider;

    var forces = [];
    var speed;
    var acceleration;

    // PUBLIC METHODS ----------------------------------------------------------------------------//
    this.setCollider = function(coll){
        collider = coll;
    }

    this.applyForce = function(force){
        // TODO
    }

    // EVENT METHODS -----------------------------------------------------------------------------//
    this.initialize = function() {}
    this.update = function(deltaTime) {}
    this.render = function(renderer) {}

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    owner = actor;

    // INTERNAL METHODS --------(Use that instead of this)----------------------------------------//
    function updateAcceleration(deltaTime){

    }
}
