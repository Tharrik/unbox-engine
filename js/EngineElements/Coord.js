class Coord{
    /** Returns a random Coord between max and min. */
    static random(max, min) {
        // Failsafe
        if(!max) {
            Coord.log.d("random (static): needs at least one value.");
            return;
        }
        
        if(!min) { min = new Coord(0, 0, 0)}

        if(!(max instanceof Coord) || !(min instanceof Coord)) {
            Coord.log.d("random (static): the values need to be Coord instances.");
            return;
        }
        // Failsafe end
        
        // Values
        let x, y, z;
        x = min.x + Math.random() * max.x;
        if(min.y !== undefined && max.y !== undefined) { y = min.y + Math.random() * max.y; }
        if(min.z !== undefined && max.z !== undefined) { z = min.z + Math.random() * max.z; }

        return new Coord(x, y, z);
    }

    constructor(x, y, z) {
        if(x !== undefined) this.x = x;
        if(y !== undefined) this.y = y;
        if(z !== undefined) this.z = z;
    }

    /** Returns a copy of this Coord. */
    copy() { return new Coord(this.x, this.y, this.z); }

    /** Sets the values of this Coord and returns it. */
    set(x, y, z) {
        if(x !== undefined && typeof(x) == "number") this.x = x;
        if(y !== undefined && typeof(x) == "number") this.y = y;
        if(z !== undefined && typeof(x) == "number") this.z = z;
        return this;
    }

    /** Adds other Coord to this and returns it as a new Coord. */
    add(other) {
        let coord = this.copy();
        if(other === undefined || !(other instanceof Coord)) {
            Coord.log.d("add (instance): the value is not a Coord instance.");
            return coord;
        }

        // X axis
        coord.x += other.x;

        // Y axis: if one of the to values is undefined set it to 0. If both are, there is no y value.
        if(coord.y !== undefined || other.y !== undefined) {
            if (coord.y === undefined) coord.y = other.y;
            else if ( other.y !== undefined) coord.y += other.y;
        }

        // Z axis: if one of the to values is undefined set it to 0. If both are, there is no z value.
        if(coord.z !== undefined || other.z !== undefined) {
            if (coord.z === undefined) coord.z = other.z;
            else if ( other.z !== undefined) coord.z += other.z;
        }

        return coord;
    }

    /** Subtracts other Coord to this and returns it as a new Coord. */
    subtract(other) {
        let coord = this.copy();
        if(other === undefined || !(other instanceof Coord)) {
            Coord.log.d("subtract (instance): the value is not a Coord instance.");
            return coord;
        }

        // X axis
        coord.x -= other.x;

        // Y axis: if one of the to values is undefined set it to 0. If both are, there is no y value.
        if(coord.y !== undefined || other.y !== undefined) {
            if (coord.y === undefined) coord.y = -other.y;
            else if ( other.y !== undefined) coord.y -= other.y;
        }

        // Z axis: if one of the to values is undefined set it to 0. If both are, there is no z value.
        if(coord.z !== undefined || other.z !== undefined) {
            if (coord.z === undefined) coord.z = -other.z;
            else if ( other.z !== undefined) coord.z -= other.z;
        }

        return coord;
    }

    /** Multiplies this Coord by a numeric factor and returns it as a new Coord. */
    multiply(factor) {
        var coord = this.copy();

        if(typeof(factor) != "number") {
            Coord.log.d("multiply (instance): the value is not a number.");
            return coord;
        }

        coord.x = this.x * factor;
        if(coord.y !== undefined) coord.y *= factor;
        if(coord.z !== undefined) coord.z *= factor;
        
        return coord;
    }

    /** Returns the module of this Coord. */
    module() {
        if(this.y === undefined) { return this.x; }
        if(this.z === undefined) { return Math.sqrt(this.x * this.x + this.y * this.y); }
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    /** Returns a normalized version of this Coord. */
    normalize(){
        return this.multiply(1/this.module());
    }

    /** Returns a string represening this Coord. */
    toString(){
        if(this.y === undefined) { return "(".concat(this.x, ")"); }
        if(this.z === undefined) { return "(".concat(this.x, ", ", this.y, ")"); }
        return "(".concat(this.x, ", ", this.y, ", ", this.z, ")");
    }
}

Coord.log = new Log("COORD");
