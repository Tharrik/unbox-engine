function Log(logger_name) {
    // STATIC MEMBERS ----------------------------------------------------------------------------//
    
    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var name = logger_name;

    // PUBLIC METHODS ----------------------------------------------------------------------------//
    this.d = function(message) {
        console.debug(name + ": " + message);
    }
}