/**
 * This class represents the background of the scenario.
 */

function Background(renderer){
    // STATIC MEMBERS ----------------------------------------------------------------------------//

    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    this.bgColor = {
        top: "#000011",
        bottom: "#c299ff"
    };

    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var bgRenderer;

    var log = new Log("BACKGROUND");

    var that = this;

    var rain;

    // PUBLIC METHODS ----------------------------------------------------------------------------//

    // EVENT METHODS -----------------------------------------------------------------------------//
    this.initialize = function() {
        rain = new Rain();
    }

    this.update = function(deltaTime) {
        rain.update(deltaTime);
    }

    this.render = function() {
        // Render sky layer
        renderSky();

        // Render rain layer
        rain.render(renderer);
    }

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    if(!renderer) {
        log.d("The background needs a renderer!")
        return undefined;
    }
    bgRenderer = renderer;

    // INTERNAL METHODS --------------------------------------------------------------------------//
    function renderSky(){
        renderer.save();
        var gradient = renderer.createLinearGradient(0, 0, 0, renderer.size.y);
        gradient.addColorStop(0, that.bgColor.top);
        gradient.addColorStop(0.25, that.bgColor.top);
        gradient.addColorStop(1, that.bgColor.bottom);
        renderer.fillStyle = gradient;
        renderer.fillRect(0, 0, renderer.size.x, renderer.size.y);
        renderer.restore();
    }

    

}

