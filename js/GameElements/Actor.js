/** This class represents the actors of the scene.
 * @param {Coord} position The initial position of the actor.
 * @param {Coord} size The physical size of the actor.
 * @param {Coord} anchor The anchor reference of the actor.
 */
function Actor(position, size, anchor){
    // STATIC MEMBERS ----------------------------------------------------------------------------//

    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    this.position;
    this.size;
    this.anchor;

    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var that = this;

    var debugColor = "red";
    
    var animations = [];
    var animStateEnum = {};
    var animState = "IDLE";
    
    // PUBLIC METHODS ----------------------------------------------------------------------------//
    this.addAnimation = function(name, img, refreshRate, size) {
        animStateEnum[name] = Object.keys(animStateEnum).length;
        animations.push(new Animation(img, refreshRate, size));
    }

    // EVENT METHODS -----------------------------------------------------------------------------//
    this.initialize = function() {
    }

    this.update = function(deltaTime) {
        animations[animStateEnum[animState]].update(deltaTime);
    }
    
    this.render = function(renderer) {
        renderer.save();

        var renderPosition = this.position.subtract(this.anchor);
        animations[animStateEnum[animState]].render(renderer, renderPosition, this.size);

        if(renderer.debug) renderDebug(renderer);

        renderer.restore();
    }

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    this.position = position;
    this.size = size;
    this.anchor = anchor;

    // INTERNAL METHODS --------------------------------------------------------------------------//

    // Draws debug elements for testing purposes.
    function renderDebug(renderer) { 
        renderer.save();

        // Render position
        renderer.strokePoint(that.position.x, that.position.y, 12, debugColor);

        // Render size box
        var renderPosition = that.position.subtract(that.anchor);
        renderer.strokeStyle = debugColor;
        renderer.strokeRect(renderPosition.x, renderPosition.y, that.size.x, that.size.y);

        renderer.restore();
    }

}
