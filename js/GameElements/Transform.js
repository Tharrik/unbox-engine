// This class represents the basic transform element used to manipulate game actors
function Transform(position, rotation, scale) {
    this.position = position != undefined && position instanceof Coord ? position : new Coord();
    this.rotation = rotation != undefined ? rotation : 0;
    this.scale    = scale    != undefined ? scale : 0;
}

//Transform.prototype.