
/** This class represents the rain of the background. */
function Rain(bounds) {
    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    this.bounds;

    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var raindrops = [];
    var dropsToErase = [];
    var maxRaindrops = 1000;
    var rainDirection = new Coord(-0.5, 1);
    var timeSinceLastDrop = 0;
    var dropRate = 0.002;

    var rainSound = new Sound("assets/sound/rain.mp3");
    rainSound.sound.loop = true;

    var log = new Log("RAIN");

    var that = this;

    // PUBLIC METHODS ----------------------------------------------------------------------------//

    // EVENT METHODS -----------------------------------------------------------------------------//
    this.update = function(deltaTime) {
        if(!this.bounds || !deltaTime) { return; }
        timeSinceLastDrop += deltaTime;
        while(timeSinceLastDrop > dropRate) {
            createDrop();
            timeSinceLastDrop -= dropRate;
        }
        for(var i = 0; i < raindrops.length; ++i) {
            raindrops[i].update(deltaTime);
            if(!this.bounds.contains(raindrops[i].position)){
                dropsToErase.push(raindrops[i]);
            }
        }
        eraseDrops();
    }

    this.render = function(renderer) {
        if(!renderer || !(renderer instanceof Renderer)) {
            log.d("Renderer needed to render!");
            return;
        }
        if(!this.bounds) {
            this.bounds = new Rectangle(new Coord(0,0), renderer.size.copy());
            expandBounds();
        }
        for(var i = 0; i < raindrops.length; ++i) {
            raindrops[i].render(renderer);
        }
    }

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    if (bounds && bounds instanceof Rectangle) {
        this.bounds = bounds.copy();
        expandBounds();
    } else {
        log.d("Bounds needed! The bounds will be the renderer's.");
    }
    rainSound.play();

    // INTERNAL METHODS --------------------------------------------------------------------------//
    function createDrop() {
        var position = that.bounds.position.copy();
        if(rainDirection.x <= 0) {
            position.x +=
                Math.random() * (that.bounds.size.x - rainDirection.x * that.bounds.size.y);
        } else {
            position.x +=
                Math.random() * (that.bounds.size.x + rainDirection.x * that.bounds.size.y);
        }
        var drop = new Raindrop(raindrops.length, position, rainDirection);
        raindrops.push(drop);
    }

    function eraseDrops() {
        while(dropsToErase.length > 0) {
            raindrops.splice(raindrops.indexOf(dropsToErase.pop()),1);
        }
        //log.d("Number of raindrops: " + raindrops.length);
    }

    function expandBounds() {
        if(rainDirection.x <= 0) {
            that.bounds.size.x -= rainDirection.x * that.bounds.size.y;
        } else {
            that.bounds.position.x -= rainDirection.x * that.bounds.size.y;
        }
    }
}

/** This is a simple raindrop. */
function Raindrop(index, position, direction) {
    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    this.position;

    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var fallingSpeed = 750 + Math.random() * 750;
    var length = 15;
    var fallingDirection;

    var log = new Log("RAINDROP " + index);

    // PUBLIC METHODS ----------------------------------------------------------------------------//
    this.setDirection = function(direction) {
        if(!(direction instanceof Coord)) { return; }
        fallingDirection = direction;
    }

    // EVENT METHODS -----------------------------------------------------------------------------//
    this.update = function(deltaTime){
        this.position = this.position.add(fallingDirection.multiply(deltaTime * fallingSpeed));
    }

    this.render = function(renderer){
        renderer.save();

        renderer.beginPath();
        renderer.moveTo(this.position.x, this.position.y);
        renderer.lineTo(
            this.position.x - fallingDirection.x * length,
            this.position.y - fallingDirection.y * length);
        var gradient = renderer.createLinearGradient(
            this.position.x, this.position.y,
            this.position.x - fallingDirection.x * length,
            this.position.y - fallingDirection.y * length);
        gradient.addColorStop(0, "white");
        gradient.addColorStop(1, "rgba(255, 255, 255, 0)");

        renderer.strokeStyle = gradient;
        renderer.lineWidth = 2;
        renderer.stroke();

        renderer.restore();
    }

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    this.position = position ? position : new Coord(100, 100);
    fallingDirection = direction ? direction.normalize() : (new Coord(1,1)).normalize();

    // INTERNAL METHODS --------------------------------------------------------------------------//
}
