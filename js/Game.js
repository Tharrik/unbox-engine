
function Game(){

    // STATIC MEMBERS ----------------------------------------------------------------------------//

    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    
    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var canvas = document.getElementById("game_canvas");
    var context = canvas.getContext("2d");
    var interval;
    var images = {};
    
    var renderer = new Renderer(new Coord(1600, 900));
    var background = new Background(renderer);
    var foreground = new Foreground(renderer);

    var player;

    var time = 0;
    var frames = 0;
    var frameTime = 0;
    var FPS = 0;

    // PUBLIC METHODS ----------------------------------------------------------------------------//

    // EVENT METHODS -----------------------------------------------------------------------------//
    

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    initialize();

    // INTERNAL METHODS --------------------------------------------------------------------------//
    function loader() {
        PreparePhysics(context);
        
        player = new Actor(renderer.center(), new Coord(80, 80), new Coord(40, 80));
        player.addAnimation("IDLE", images.playerIdle, 8, new Coord(86,86));
        player.initialize();

        run();
    }

    function initialize() {
        window.requestAnimationFrame = (function (evt) {
            return window.requestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.msRequestAnimationFrame ||
                function (callback) {
                    window.setTimeout(callback, 16.6666666);
                };
        }) ();

        renderer.initialize();
        background.initialize();
        foreground.initialize();

        images["playerIdle"] = new Image();
        images.playerIdle.src = "./assets/img/char_nuclear.png";
        images.playerIdle.onload = loader;
    }

    function run(){
        requestAnimationFrame(run);

        var now = Date.now();
        var deltaTime = now - time;
        if(deltaTime > 1000) { deltaTime = 0; }
        time = now;
        ++frames;
        frameTime += deltaTime;
        if(frameTime > 100) {
            FPS = frames;
            frames = 0;
            frameTime -= 1000;
        }
        deltaTime /= 1000;

        update(deltaTime);
        render();
    }

    function update(deltaTime) {

        world.Step(deltaTime, 8, 3);
        world.ClearForces();

        background.update(deltaTime);
        foreground.update(deltaTime);
        player.update(deltaTime);
    }

    function render(){
        background.render();
        foreground.render();
        //DrawWorld(world);
        player.render(renderer);
        DrawWorld(world);
        
    }

    function DrawWorld (world) {
        // Transform the canvas coordinates to cartesian coordinates
        context.save();
        context.translate(0, canvas.height);
        context.scale(1, -1);
        world.DrawDebugData();
        context.restore();
    }

}


